#/bin/sh

SWIRLY_DRIVER=./indi_swirly_flipflat

trap 'kill $(jobs -p)' EXIT

(
	cd ../../build
	make || exit 1

	rm debug.txt

	indiserver -r 1 indi_simulator_ccd indi_simulator_telescope ${SWIRLY_DRIVER} 2>&1 > indiserver.log &
	#indiserver -r 1 indi_gphoto_ccd indi_eqmod_telescope ../../build/indi_swirly 2>&1 > indiserver.log &
	indipid=$?
	socat -d -d pty,link=/tmp/ttySwirly,raw,echo=0 EXEC:"python ../Swirly/test/dummy.py" &
	socatpid=$?

	#read foobar
	tail -F debug.txt

	# kill ${indipid} ${socatpid}
)
