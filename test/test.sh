#/bin/sh

trap 'kill $(jobs -p)' EXIT

indiserver -r 1 indi_simulator_ccd indi_simulator_telescope ../../build/indi_swirly 2>&1 > indiserver.log
