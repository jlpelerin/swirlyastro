import sys
from astrometry.util._util import TRUE

class Device(object):
    def __init__(self, logger):
        self.logger = logger

    def execute(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'G':
            return self.cmdGet(cmd)
        elif c == 'S':
            return self.cmdSet(cmd)
        elif c == 'F':
            return self.cmdFunc(cmd)

class Focuser(Device):
    def __init__(self, logger):
        super(Focuser, self).__init__(logger)
        self.position = 25000
        self.target = 25000
        self.speed = 5
        self.backlash = 0

    def cmdGet(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'H':
            return "00#"
        if c == 'V':
            return "01#"
        if c == 'P':
            return "{:x}#".format(self.position)
        if c == 'D':
            return "{:x}#".format(self.speed)
        if c == 'I':
            return "0#" if self.position == self.target else "1#"
        if c == 'B':
            return "{:x}#".format(self.backlash)

    def cmdSet(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'N':
            self.position = int(cmd[:-1], 16)
            return ""
        if c == 'H':
            return ""
        if c == 'F':
            return ""
        if c == 'D':
            self.speed = int(cmd[:-1], 16)
            return ""
        if c == 'B':
            self.backlash = int(cmd[:-1], 16)
            return ""

    def cmdFunc(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'Q':
            self.target = self.position
            return ""

class DustCap(Device):
    def __init__(self, logger):
        super(DustCap, self).__init__(logger)
        self.parked = True

    def cmdFunc(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'P':
            if(not self.parked):
                self.logger.write("Cap PARKED\n")
                self.logger.flush()
            self.parked = True
            return ""
        if c == 'U':
            if(self.parked):
                self.logger.write("Cap UNPARKED\n")
                self.logger.flush()
            self.parked = False
            return ""

    def cmdGet(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'P':
            return "1#" if self.parked else "0#"

class LightBox(Device):
    def __init__(self, logger):
        super(LightBox, self).__init__(logger)
        self.lighton = False

    def cmdSet(self, cmd):
        c = cmd[0]
        cmd = cmd[1:]
        if c == 'L':
            if(cmd[:1] == '1'):
                if(not self.lighton):
                    self.logger.write("Light ON\n")
                    self.logger.flush()
                self.lighton = True
            else:
                if(self.lighton):
                    self.logger.write("Light OFF\n")
                    self.logger.flush()
                self.lighton = False
        return ""

class SwirlyDummy:
    def __init__(self, logger):
        self.logger = logger       
        self.focuser = Focuser(logger)
        self.dustcap = DustCap(logger)
        self.lightbox = LightBox(logger)

    def start(self):
        cmd=""
        while True:    
            c = sys.stdin.read(1)
            if c == "\n": continue
            cmd += c
            if c == "#":
#                self.logger.write(cmd+"\n")
#                self.logger.flush()
                
                # Execute command and send results
                sys.stdout.write(self.execute(cmd))
                sys.stdout.flush()
                cmd = ""

    def execute(self, cmd):
        device = cmd[0]
        cmd = cmd[1:]
        if(device == ':'): # Focuser
            return self.focuser.execute(cmd)
        if(device == 'C'): # Cap
            return self.dustcap.execute(cmd)
        if(device == 'L'):
            return self.lightbox.execute(cmd)

with open("debug.txt", "w") as f:
    dummy = SwirlyDummy(f)
    dummy.start()
