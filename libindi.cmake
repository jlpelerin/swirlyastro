# - Try to find libindi
# Once done this will define
#  LIBINDI_FOUND - System has LibIndi
#  LIBINDI_INCLUDE_DIRS - The LibIndi include directories
#  LIBINDI_LIBRARIES - The libraries needed to use LibIndi
#  LIBINDI_DEFINITIONS - Compiler switches required for using LibIndi

find_package(PkgConfig)
pkg_check_modules(PC_LIBINDI REQUIRED libindi)
set(LIBINDI_DEFINITIONS ${PC_LIBINDI_CFLAGS_OTHER})

find_path(LIBINDI_INCLUDE_DIR libindi/indidriver.h
          HINTS ${PC_LIBINDI_INCLUDEDIR} ${PC_LIBINDI_INCLUDE_DIRS}
          PATH_SUFFIXES libindi )

find_library(LIBINDI_LIBRARY NAMES indidriver libindidriver
             HINTS ${PC_LIBINDI_LIBDIR} ${PC_LIBINDI_LIBRARY_DIRS} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBINDI_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibIndi  DEFAULT_MSG
                                  LIBINDI_LIBRARY LIBINDI_INCLUDE_DIR)

mark_as_advanced(LIBINDI_INCLUDE_DIR LIBINDI_LIBRARY)

set(LIBINDI_LIBRARIES ${LIBINDI_LIBRARY} )
set(LIBINDI_INCLUDE_DIRS ${LIBINDI_INCLUDE_DIR} )

message(STATUS "LIBINDI_LIBRARY=${LIBINDI_LIBRARY}")
