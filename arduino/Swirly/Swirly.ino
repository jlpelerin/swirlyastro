#include <Servo.h>

// 0 -> Focuser
// 1 -> Flip-Flat
#define SWIRLY_TYPE 1

#define VERSION_STRING "01#"

// Debugging commands:
// - LIGHT:
//     On:  LFO#
//     Off: LFX#
//     Get status: LGS#
// - CAP:
//     Open:  CFO#
//     Close: CFC#
//     Get motor status: CGM#
//     Get cap status: CGS#
// - FOCUSER:
//     Position: FSN0000#  //  FSN61a8#
//     Abort:    FFQ#

#define RESPOND Serial.print

#define PREFIX_LIGHT   'L'
#define PREFIX_DUSTCAP 'C'
#define PREFIX_FOCUSER 'F'

#define PREFIX_GET  'G'
#define PREFIX_SET  'S'
#define PREFIX_FUNC 'F'

#define PIN_LIGHT_1   10
#define PIN_DUSTCAP_1 11

#define PIN_MOTOR_1   4
#define PIN_MOTOR_2   5
#define PIN_MOTOR_3   6
#define PIN_MOTOR_4   7

class Device {
public:
    virtual void cmdGet(char c) = 0;
    virtual void cmdSet(char *s) = 0;
    virtual void cmdFunc(char c) = 0;
};

class Focuser : public Device {
  public:
    Focuser();

    void loop(void);
private:
    void cmdGet(char c);
    void cmdSet(char *s);
    void cmdFunc(char c);
    
    void counterclockwise();
    void clockwise();
    void updateDelay();

    const static int MAX_DELAY = 1;
    const static int PIN_DELAY = 8;

  //declare variables for the motor pins
    static const int motorPin1 = PIN_MOTOR_1;	// Blue   - 28BYJ48 pin 1
    static const int motorPin2 = PIN_MOTOR_2;	// Pink   - 28BYJ48 pin 2
    static const int motorPin3 = PIN_MOTOR_3;	// Yellow - 28BYJ48 pin 3
    static const int motorPin4 = PIN_MOTOR_4;	// Orange - 28BYJ48 pin 4
                       // Red    - 28BYJ48 pin 5 (VCC)

    int focuserSpeed = 5;     //variable to set stepper speed
    int focuserDelay = 8;
    int focuserTarget = 25000;
    int focuserPos = 25000;
    int focuserBacklashTarget = -1; // Save actual target
    int focuserBacklashOffset = 25;
};

class DustCap : public Device {
  typedef enum {DUSTCAP_OPEN, DUSTCAP_CLOSED} DustCapState;

  public:
    DustCap();

    void loop(void);
  private:
    void cmdGet(char c);
    void cmdSet(char *s);
    void cmdFunc(char c);
    
    void Open();
    void Close();
    
    static const int servoPin = PIN_DUSTCAP_1;
    Servo *servo;

    DustCapState targetState, currentState;
    int targetPosition, currentPosition;
    int openPosition = 0;
    int closedPosition = 168;

    static const int stepSize = 2;
    static const int capDelay = 10;
};

class LightBox : public Device {
  public:
    LightBox();

    void loop(void);
  private:
    void cmdGet(char c);
    void cmdSet(char *s);
    void cmdFunc(char c);
    
    void switchOn();
    void switchOff();
    
    static const int lightPin = PIN_LIGHT_1;
    boolean isOn = false;
};

//////////////////////////////////////////////////////////////////////////////

#if SWIRLY_TYPE == 0
Focuser *focuser = NULL;
#elif SWIRLY_TYPE == 1
DustCap *dustcap = NULL;
LightBox *lightbox = NULL;
#endif

void setup() {
 Serial.begin(9600);
 while ( !Serial ) {
   // Wait until Serial is available.
 }
 Serial.setTimeout(50);
 Serial.flush(); 
 
#if SWIRLY_TYPE == 0
 focuser = new Focuser();
#elif SWIRLY_TYPE == 1 
 dustcap = new DustCap();
 lightbox = new LightBox();
#endif
}

void loop(){
  // Get incoming command.  
  String inLine = Serial.readStringUntil('#');

  if(inLine.length() == 0) {
#if SWIRLY_TYPE == 0
    focuser->loop();
#elif SWIRLY_TYPE == 1
    dustcap->loop();
    lightbox->loop();
#endif
  } else {
    // TODO: Check inLine length is sufficient

    Device *device = NULL;
    switch(inLine[0]) {
#if SWIRLY_TYPE == 0
      case PREFIX_FOCUSER:
        device = focuser;
        break;
#elif SWIRLY_TYPE == 1
      case PREFIX_DUSTCAP:
        device = dustcap;
        break;
      case PREFIX_LIGHT:
        device = lightbox;
        break;
#endif
      default:
        return;
    }

    switch(inLine[1]) {
      case PREFIX_GET:
        device->cmdGet(inLine[2]);
        break;
      case PREFIX_SET:
        device->cmdSet(&inLine[2]);
        break;
      case PREFIX_FUNC:
        device->cmdFunc(inLine[2]);
        break;
    }
  }
}
//////////////////////////////////////////////////////////////////////////////

/**********************************
 * FOCUSER
 **********************************/

Focuser::Focuser() {
 //declare the motor pins as outputs
 pinMode(motorPin1, OUTPUT);
 pinMode(motorPin2, OUTPUT);
 pinMode(motorPin3, OUTPUT);
 pinMode(motorPin4, OUTPUT);

 updateDelay();
}

//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 1 to 4
//delay "focuserSpeed" between each pin setting (to determine speed)

void
Focuser::counterclockwise () {
 // 1
 digitalWrite(motorPin1, HIGH);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin4, LOW);
 delay(PIN_DELAY);
 // 2
 digitalWrite(motorPin1, HIGH);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin4, LOW);
 delay (PIN_DELAY);
 // 3
 digitalWrite(motorPin1, LOW);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin4, LOW);
 delay(PIN_DELAY);
 // 4
 digitalWrite(motorPin1, LOW);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin4, LOW);
 delay(PIN_DELAY);
 // 5
 digitalWrite(motorPin1, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin4, LOW);
 delay(PIN_DELAY);
 // 6
 digitalWrite(motorPin1, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin4, HIGH);
 delay (PIN_DELAY);
 // 7
 digitalWrite(motorPin1, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin4, HIGH);
 delay(PIN_DELAY);
 // 8
 digitalWrite(motorPin1, HIGH);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin4, HIGH);
 delay(PIN_DELAY);

 delay(focuserDelay);
 focuserPos--;
}

void
Focuser::updateDelay() {
  focuserDelay = MAX_DELAY << (5 - focuserSpeed);
}

//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 4 to 1
//delay "focuserSpeed" between each pin setting (to determine speed)

void
Focuser::clockwise(){
 // 1
 digitalWrite(motorPin4, HIGH);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin1, LOW);
 delay(PIN_DELAY);
 // 2
 digitalWrite(motorPin4, HIGH);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin1, LOW);
 delay (PIN_DELAY);
 // 3
 digitalWrite(motorPin4, LOW);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin1, LOW);
 delay(PIN_DELAY);
 // 4
 digitalWrite(motorPin4, LOW);
 digitalWrite(motorPin3, HIGH);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin1, LOW);
 delay(PIN_DELAY);
 // 5
 digitalWrite(motorPin4, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin1, LOW);
 delay(PIN_DELAY);
 // 6
 digitalWrite(motorPin4, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin2, HIGH);
 digitalWrite(motorPin1, HIGH);
 delay (PIN_DELAY);
 // 7
 digitalWrite(motorPin4, LOW);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin1, HIGH);
 delay(PIN_DELAY);
 // 8
 digitalWrite(motorPin4, HIGH);
 digitalWrite(motorPin3, LOW);
 digitalWrite(motorPin2, LOW);
 digitalWrite(motorPin1, HIGH);
 delay(PIN_DELAY);

 delay(focuserDelay);
 focuserPos++;
}

void
Focuser::loop(void) {    
  if(focuserPos < focuserTarget) {
    clockwise();
  } else if(focuserPos > focuserTarget) {
    counterclockwise();
  } else { // focuserPos == focuserTarget
    if(focuserBacklashTarget != -1) {
      focuserTarget = focuserBacklashTarget;
      focuserBacklashTarget = -1;
    }
  }
}

void
Focuser::cmdGet(char c) {
  char res[32];
  switch(c) {
    case 'H':
      // Read step mode
      RESPOND("00#");
      break;
    case 'V':
      // Read version
      RESPOND(VERSION_STRING);
      break;
    case 'P':
      // Read position
      sprintf(res, "%X#", focuserPos);
      RESPOND(res);
      break;
    case 'D':
      // Read speed
      sprintf(res, "%X#", focuserSpeed);
      RESPOND(res);
      break;
    case 'I':
      // Is moving
      if ((focuserBacklashTarget == -1) && (focuserPos == focuserTarget))
        RESPOND("0#");
      else
        RESPOND("1#");
      break;
    case 'B':
      // Read backlash
      sprintf(res, "%X#", focuserBacklashOffset);
      RESPOND(res);
      break;
  }
}

void
Focuser::cmdSet(char *s) {
  switch(s[0]) {
    case 'N':
      // Set position
      sscanf(&s[1], "%x", &focuserTarget);

      // Deal with backlash
      if ((focuserPos > focuserTarget) && (focuserBacklashOffset > 0)) {
        focuserBacklashTarget = focuserTarget;
        focuserTarget -= focuserBacklashOffset;
      }

      break;
    case 'H':
      // Set 'half' step mode
      break;
    case 'F':
      // Set 'full' step mode
      break;
    case 'D':
      // Set speed
      sscanf(&s[1], "%x", &focuserSpeed);
      updateDelay();
      printf("%d", focuserDelay);
      break;
    case 'B':
      // Set backlash
      sscanf(&s[1], "%x", &focuserBacklashOffset);
      break;
  }
}

void
Focuser::cmdFunc(char c) {
  switch(c) {
    case 'Q':
      // Abort
      focuserBacklashTarget = -1;
      focuserTarget = focuserPos;
      break;
  }
}

/**********************************
 * DUST CAP
 **********************************/

DustCap::DustCap() {
  servo = new Servo();
  servo->attach(servoPin);  
  Open();

  servo->write(targetPosition);
  currentPosition = targetPosition;
}

void
DustCap::loop(void) {
  if (currentPosition > targetPosition) {
    currentPosition = max(currentPosition - stepSize, targetPosition);
  } else if (currentPosition < targetPosition) {
    currentPosition = min(currentPosition + stepSize, targetPosition);
  } else {
    return;
  }
  servo->write(currentPosition);
  delay(capDelay);
}

void
DustCap::cmdGet(char c) {
  switch(c) {
    case 'V':
      // Read version
      RESPOND(VERSION_STRING);
      break;
    case 'S':
      // Read status
      if (currentPosition == targetPosition) {
        if (currentPosition == openPosition) {
          RESPOND("0#");
        } else if (currentPosition == closedPosition) {
          RESPOND("1#");
        } else {
          RESPOND("2#");
        }
      } else {
        RESPOND("2#");
      }
      break;
    case 'M':
      // Read if moving
      if (currentPosition == targetPosition) RESPOND("0#");
      else RESPOND("1#");
      break;
  }
}

void
DustCap::cmdSet(char *s) {
  switch(s[0]) {
    case 'O':
      // Set open position
      sscanf(&s[1], "%x", &openPosition);
      break;
    case 'C':
      // Set open position
      sscanf(&s[1], "%x", &closedPosition);
      break;
  }
}

void
DustCap::cmdFunc(char c) {
  switch(c) {
    case 'O':
      Open();
      break;
    case 'C':
      Close();
      break;
  }
}

void
DustCap::Open() {
  targetPosition = openPosition;
}

void
DustCap::Close() {
  targetPosition = closedPosition;
}

/**********************************
 * LIGHT BOX
 **********************************/

LightBox::LightBox() {
  pinMode(lightPin, OUTPUT);
  
  switchOff();
}

void
LightBox::loop(void) {
}

void
LightBox::cmdGet(char c) {
  char res[32];
  switch(c) {
    case 'S':
      // Read status
      if (isOn) RESPOND("1#");
      else RESPOND("0#");
      break;
  }
}

void
LightBox::cmdSet(char *s) {
}

void
LightBox::cmdFunc(char c) {
  switch(c) {
    case 'O':
      switchOn();
      break;
    case 'X':
      switchOff();
      break;
  }
}

void
LightBox::switchOn() {
  digitalWrite(lightPin, HIGH);
  isOn = true;
}

void
LightBox::switchOff() {
  digitalWrite(lightPin, LOW);
  isOn = false;
}

