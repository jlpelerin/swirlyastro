/*
 * Swirly_utils.h
 *
 *  Created on: 5 oct. 2020
 *      Author: jep
 */

#ifndef SRC_SWIRLYDEVICE_H_
#define SRC_SWIRLYDEVICE_H_

#include "defaultdevice.h"

#include <stdint.h>
#include <iostream>
#include <fstream>

class SwirlyDevice :
		public INDI::DefaultDevice
{
public:
	SwirlyDevice();
protected:
	/**
	 * @brief sendCommand Send a string command to Swirly.
	 * @param cmd Command to be sent, must already have the necessary delimeter ('#')
	 * @param res If not nullptr, the function will read until it detects the default delimeter ('#') up to ML_RES length.
	 *        if nullptr, no read back is done and the function returns true.
	 * @param silent if true, do not print any error messages.
	 * @param nret if > 0 read nret chars, otherwise read to the terminator
	 * @return True if successful, false otherwise.
	 */
	bool sendCommand(int PortFD, const char * cmd, char * res = nullptr, int nret = 0);

	std::ofstream &debug() { return debugStream; }

protected:
    // Swirly Buffer
    static const uint8_t ML_RES { 32 };
    // Swirly Delimeter
    static const char ML_DEL { '#' };
    // Swirly Timeout
    static const uint8_t ML_TIMEOUT { 3 };

    std::ofstream debugStream;
};


#endif /* SRC_SWIRLYDEVICE_H_ */
