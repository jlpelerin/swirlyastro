/*
    Moonlite Focuser
    Copyright (C) 2013-2019 Jasem Mutlaq (mutlaqja@ikarustech.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/* Commands format
 * <D><C><P>[<XXXX>]#
 * - <D> = Device
 *   - 'F' = Focuser
 *   - 'C' = Dust cap
 *   - 'L' = Light
 * - <C> = Command
 *   - 'S' = Set variable value
 *   - 'G' = Get variable value
 *   - 'F' = Call function
 * - <P> = Parameter. Context dependent.
 *   - For 'set' and 'get', it represents the variable to set or get
 *   - For 'function', it represents the function to call
 */

#include "Swirly_Focuser.h"

#include <indicom.h>
#include <connectionplugins/connectionserial.h>
#include <connectionplugins/connectiontcp.h>

#include <cmath>
#include <cstring>
#include <memory>

#include <termios.h>
#include <unistd.h>

static std::unique_ptr<Swirly> focuser(new Swirly());

const char *FOCUSER_TAB = "Focuser";

Swirly::Swirly() :
		SwirlyDevice(),
		FocuserInterface(this)
{
    setVersion(1, 1);

    // Can move in Absolute & Relative motions, can AbortFocuser motion, and has variable speed.
    //    FI::SetCapability(FOCUSER_CAN_ABS_MOVE | FOCUSER_CAN_REL_MOVE | FOCUSER_CAN_ABORT | FOCUSER_HAS_VARIABLE_SPEED | FOCUSER_CAN_SYNC);
    FocuserInterface::SetCapability(FOCUSER_CAN_ABS_MOVE | FOCUSER_CAN_REL_MOVE | FOCUSER_CAN_ABORT | FOCUSER_HAS_BACKLASH | FOCUSER_CAN_SYNC);
}

const char * Swirly::getDefaultName()
{
    return "Swirly Focuser";
}

bool Swirly::initProperties()
{
    LOG_INFO("Entering Swirly::initProperties");
    LOGF_INFO("  Device name = '%s'", getDeviceName());

    DefaultDevice::initProperties();

    /***
     * FOCUSER
     */
    FocuserInterface::initProperties(FOCUSER_TAB);

    FocusSpeedN[0].min   = 1;
    FocusSpeedN[0].max   = 5;
    FocusSpeedN[0].value = 1;

    /* Relative and absolute movement */
    FocusRelPosN[0].min   = 0.;
    FocusRelPosN[0].max   = 50000.;
    FocusRelPosN[0].value = 0;
    FocusRelPosN[0].step  = 1000;

    FocusAbsPosN[0].min   = 0.;
    FocusAbsPosN[0].max   = 100000.;
    FocusAbsPosN[0].value = 0;
    FocusAbsPosN[0].step  = 1000;

    FocusBacklashN[0].min = 0;
    FocusBacklashN[0].max = 500;
    FocusBacklashN[0].value = 10;
    FocusBacklashN[0].step = 1;
    strcpy(FocusBacklashN[0].format, "%3.0f");

    setDriverInterface(AUX_INTERFACE | FOCUSER_INTERFACE);

    addAuxControls();

    serialConnection = new Connection::Serial(this);
    serialConnection->registerHandshake([&]()
    {
        return callHandshake();
    });
    DefaultDevice::registerConnection(serialConnection);

    tcpConnection = new Connection::TCP(this);
    tcpConnection->registerHandshake([&]()
    {
        return callHandshake();
    });
    DefaultDevice::registerConnection(tcpConnection);

    /***
     * GENERIC
     */
    setDefaultPollingPeriod(500);
    addDebugControl();

    return true;
}

void
Swirly::ISGetProperties(const char *dev) {
    DefaultDevice::ISGetProperties(dev);
}

bool
Swirly::updateProperties()
{
    LOG_INFO("Entering Swirly::updateProperties");

    DefaultDevice::updateProperties();
    FocuserInterface::updateProperties();

    if (isConnected())
    {
        GetParams();

        LOG_INFO("Swirly parameters updated, ready for use.");
    }
    else
    {
    }

    return true;
}

void Swirly::GetParams()
{
    if (readPosition())
        IDSetNumber(&FocusAbsPosNP, nullptr);

    if (readSpeed())
        IDSetNumber(&FocusSpeedNP, nullptr);

    if (readBacklash())
        IDSetNumber(&FocusBacklashNP, nullptr);
}

bool Swirly::ISNewSwitch(const char * dev, const char * name, ISState * states, char * names[], int n)
{
    if(FocuserInterface::processSwitch(dev, name, states, names, n)) {
        LOGF_INFO("  Switch '%s' Handled by Focuser", name);
    	return true;
    }
    if(DefaultDevice::ISNewSwitch(dev, name, states, names, n)) {
        LOGF_INFO("  Switch '%s' Handled by DefaultDevice", name);
    	return true;
    }
    LOGF_INFO("  Switch '%s' unhandled", name);
    return false;
}

bool
Swirly::ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n)
{
    if(DefaultDevice::ISNewText(dev, name, texts, names, n)) {
        LOGF_INFO("  Text '%s' Handled by DefaultDevice", name);
    	return true;
    }
    LOGF_INFO("  Text '%s' unhandled", name);
    return false;
}

bool Swirly::ISNewNumber(const char * dev, const char * name, double values[], char * names[], int n)
{
    if(FocuserInterface::processNumber(dev, name, values, names, n)) {
        LOGF_INFO("  Number '%s' Handled by Focuser", name);
    	return true;
    }
    if(DefaultDevice::ISNewNumber(dev, name, values, names, n)) {
        LOGF_INFO("  Number '%s' Handled by DefaultDevice", name);
    	return true;
    }
    LOGF_INFO("  Number '%s' unhandled", name);
    return false;
}

bool
Swirly::ISNewBLOB(const char *dev, const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[], char *names[], int n) {
    if(DefaultDevice::ISNewBLOB(dev, name, sizes, blobsizes, blobs, formats, names, n)) {
        LOGF_INFO("  Blob '%s' Handled by DefaultDevice", name);
    	return true;
    }
    LOGF_INFO("  Blob '%s' unhandled", name);
    return false;
}

bool
Swirly::ISSnoopDevice(XMLEle *root) {
    LOG_INFO("Swirly::ISSnoopDevice");
    return INDI::DefaultDevice::ISSnoopDevice(root);
}

bool Swirly::saveConfigItems(FILE * fp)
{
    INDI::DefaultDevice::saveConfigItems(fp);

	return FocuserInterface::saveConfigItems(fp);
}

bool
Swirly::callHandshake()
{
    if (swirlyConnection > 0)
    {
        if (getActiveConnection() == serialConnection)
            PortFD = serialConnection->getPortFD();
        else if (getActiveConnection() == tcpConnection)
            PortFD = tcpConnection->getPortFD();
    }

    return Handshake();
}

bool Swirly::Handshake()
{
    if (Ack())
    {
        LOG_INFO("Swirly is online. Getting focus parameters...");
        return true;
    }

    LOG_INFO(
        "Error retrieving data from Swirly, please ensure Swirly controller is powered and the port is correct.");
    return false;
}

bool Swirly::Ack()
{
    bool success = false;

    for (int i = 0; i < 3; i++)
    {
        if (readVersion())
        {
            success = true;
            break;
        }

        sleep(1);
    }

    return success;
}

bool Swirly::readVersion()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "FGV#", res, 2) == false)
        return false;

    LOGF_INFO("Detected firmware version %c.%c", res[0], res[1]);

    return true;
}

bool Swirly::readPosition()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "FGP#", res) == false)
        return false;

    int32_t pos;
    int rc = sscanf(res, "%X#", &pos);

    if (rc > 0)
        FocusAbsPosN[0].value = pos;
    else
    {
        LOGF_ERROR("Unknown error: focuser position value (%s)", res);
        return false;
    }

    return true;
}

bool Swirly::readSpeed()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "FGD#", res) == false)
        return false;

    uint16_t speed = 0;
    int rc = sscanf(res, "%hX#", &speed);

    if (rc > 0) FocusSpeedN[0].value = speed;
    else {
        LOGF_ERROR("Unknown error: focuser speed value (%s)", res);
        return false;
    }

    return true;
}

bool
Swirly::readBacklash()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "FGB#", res) == false)
        return false;

    uint16_t steps = 0;
    int rc = sscanf(res, "%hX#", &steps);

    if (rc > 0) FocusBacklashN[0].value = steps;
    else {
        LOGF_ERROR("Unknown error: focuser backlash steps (%s)", res);
        return false;
    }

    return true;
}

bool Swirly::isMoving()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "FGI#", res) == false)
        return false;

    // JM 2020-03-13: 01# and 1# should be both accepted
    if (strstr(res, "1#"))
        return true;
    else if (strstr(res, "0#"))
        return false;

    LOGF_ERROR("Unknown error: isMoving value (%s)", res);
    return false;
}

bool Swirly::SyncFocuser(uint32_t ticks)
{
    char cmd[ML_RES] = {0};
    snprintf(cmd, ML_RES, "FSP%04X#", ticks);
    return sendCommand(PortFD, cmd);
}

bool Swirly::MoveFocuser(uint32_t position)
{
    char cmd[ML_RES] = {0};
    snprintf(cmd, ML_RES, "FSN%04X#", position);
    // Set Position First
    if (sendCommand(PortFD, cmd) == false)
        return false;
    // Now start motion toward position
    //    if (sendCommand(PortFD, "FFG#") == false)
    //        return false;

    return true;
}

bool
Swirly::setSpeed(int speed)
{
    char cmd[ML_RES] = {0};
    // int hex_value = 1;
    // hex_value <<= speed;
    // snprintf(cmd, ML_RES, ":SD%02X#", hex_value);
    snprintf(cmd, ML_RES, "FSD%02X#", speed);
    return sendCommand(PortFD, cmd);
}

bool
Swirly::setBacklash(int32_t steps)
{
    char cmd[ML_RES] = {0};
    snprintf(cmd, ML_RES, "FSB%04X#", steps);
    return sendCommand(PortFD, cmd);
}

bool
Swirly::SetFocuserSpeed(int speed)
{
    return setSpeed(speed);
}

bool
Swirly::SetFocuserBacklash(int32_t steps) {
	return setBacklash(steps);
}

bool
Swirly::SetFocuserBacklashEnabled(bool enabled) {
	// TODO: set steps to 0 if disabled or set back value if enabled
	return true;
}

IPState Swirly::MoveFocuser(FocusDirection dir, int speed, uint16_t duration)
{
    if (speed != static_cast<int>(FocusSpeedN[0].value))
    {
        if (!setSpeed(speed))
            return IPS_ALERT;
    }

    // either go all the way in or all the way out
    // then use timer to stop
    if (dir == FOCUS_INWARD)
        MoveFocuser(0);
    else
        MoveFocuser(static_cast<uint32_t>(FocusMaxPosN[0].value));

    IEAddTimer(duration, &Swirly::timedMoveHelper, this);
    return IPS_BUSY;
}

void Swirly::timedMoveHelper(void * context)
{
    static_cast<Swirly *>(context)->timedMoveCallback();
}

void Swirly::timedMoveCallback()
{
    AbortFocuser();
    FocusAbsPosNP.s = IPS_IDLE;
    FocusRelPosNP.s = IPS_IDLE;
    FocusTimerNP.s = IPS_IDLE;
    FocusTimerN[0].value = 0;
    IDSetNumber(&FocusAbsPosNP, nullptr);
    IDSetNumber(&FocusRelPosNP, nullptr);
    IDSetNumber(&FocusTimerNP, nullptr);
}

IPState Swirly::MoveAbsFocuser(uint32_t targetTicks)
{
    targetPos = targetTicks;

    if (!MoveFocuser(targetPos))
        return IPS_ALERT;

    return IPS_BUSY;
}

IPState Swirly::MoveRelFocuser(FocusDirection dir, uint32_t ticks)
{
    // Clamp
    int32_t offset = ((dir == FOCUS_INWARD) ? -1 : 1) * static_cast<int32_t>(ticks);
    int32_t newPosition = FocusAbsPosN[0].value + offset;
    newPosition = std::max(static_cast<int32_t>(FocusAbsPosN[0].min), std::min(static_cast<int32_t>(FocusAbsPosN[0].max), newPosition));

    if (!MoveFocuser(newPosition))
        return IPS_ALERT;

    FocusRelPosN[0].value = ticks;
    FocusRelPosNP.s       = IPS_BUSY;

    return IPS_BUSY;
}

void Swirly::TimerHit()
{
    if (!isConnected())
        return;

    // Focuser position
    bool rc = readPosition();
    if (rc)
    {
        if (fabs(lastPos - FocusAbsPosN[0].value) > 5)
        {
            IDSetNumber(&FocusAbsPosNP, nullptr);
            lastPos = static_cast<uint32_t>(FocusAbsPosN[0].value);
        }
    }

    if (FocusAbsPosNP.s == IPS_BUSY || FocusRelPosNP.s == IPS_BUSY)
    {
        if (!isMoving())
        {
            FocusAbsPosNP.s = IPS_OK;
            FocusRelPosNP.s = IPS_OK;
            IDSetNumber(&FocusAbsPosNP, nullptr);
            IDSetNumber(&FocusRelPosNP, nullptr);
            lastPos = static_cast<uint32_t>(FocusAbsPosN[0].value);
            LOG_INFO("Focuser reached requested position.");
        }
    }

    SetTimer(POLLMS);
}

bool Swirly::AbortFocuser()
{
    return sendCommand(PortFD, "FFQ#");
}

