/*
    Moonlite Focuser
    Copyright (C) 2013-2019 Jasem Mutlaq (mutlaqja@ikarustech.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#pragma once

#include <libindi/defaultdevice.h>
#include <libindi/indifocuserinterface.h>
#include <libindi/indilightboxinterface.h>
#include <libindi/indidustcapinterface.h>

#include <chrono>

#include "SwirlyDevice.h"

namespace Connection
{
class Serial;
class TCP;
}

class Swirly :
		public SwirlyDevice,
		public INDI::FocuserInterface
{
    public:
    	Swirly();
        virtual ~Swirly() override = default;

        /** \struct FocuserConnection
                \brief Holds the connection mode of the Focuser.
            */
        typedef enum
        {
            CONNECTION_NONE   = 1 << 0, /** Do not use any connection plugin */
            CONNECTION_SERIAL = 1 << 1, /** For regular serial and bluetooth connections */
            CONNECTION_TCP    = 1 << 2  /** For Wired and WiFI connections */
        } FocuserConnection;

//        typedef enum { FOCUS_HALF_STEP, FOCUS_FULL_STEP } FocusStepMode;

        const char * getDefaultName() override;
        virtual bool initProperties() override;
        virtual void ISGetProperties(const char *dev) override;
        virtual bool updateProperties() override;
        virtual bool ISNewNumber(const char * dev, const char * name, double values[], char * names[], int n) override;
        virtual bool ISNewSwitch(const char * dev, const char * name, ISState * states, char * names[], int n) override;
        virtual bool ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n) override;
        virtual bool ISNewBLOB(const char *dev, const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[], char *names[], int n) override;
        virtual bool ISSnoopDevice(XMLEle *root) override;

        /***
         * FOCUSER
         */
        static void timedMoveHelper(void * context);
    protected:
        Connection::Serial *serialConnection = nullptr;
        Connection::TCP *tcpConnection       = nullptr;

        virtual void TimerHit() override;
        virtual bool saveConfigItems(FILE * fp) override;

        /***
         * FOCUSER
         */
        /**
         * @brief Handshake Try to communicate with Focuser and see if there is a valid response.
         * @return True if communication is successful, false otherwise.
         */
        virtual bool Handshake();

        /**
         * @brief MoveFocuser Move focuser in a specific direction and speed for period of time.
         * @param dir Direction of motion. Inward or Outward
         * @param speed Speed of motion
         * @param duration Timeout in milliseconds.
         * @return IPS_BUSY if motion is in progress. IPS_OK if motion is small and already complete. IPS_ALERT for trouble.
         */
        virtual IPState MoveFocuser(FocusDirection dir, int speed, uint16_t duration) override;

        /**
         * @brief MoveAbsFocuser Move to an absolute target position
         * @param targetTicks target position
         * @return IPS_BUSY if motion is in progress. IPS_OK if motion is small and already complete. IPS_ALERT for trouble.
         */
        virtual IPState MoveAbsFocuser(uint32_t targetTicks) override;

        /**
         * @brief MoveRelFocuser Move focuser for a relative amount of ticks in a specific direction
         * @param dir Directoin of motion
         * @param ticks steps to move
         * @return IPS_BUSY if motion is in progress. IPS_OK if motion is small and already complete. IPS_ALERT for trouble.
         */
        virtual IPState MoveRelFocuser(FocusDirection dir, uint32_t ticks) override;

        /**
         * @brief SyncFocuser Set the supplied position as the current focuser position
         * @param ticks target position
         * @return IPS_OK if focuser position is now set to ticks. IPS_ALERT for problems.
         */
        virtual bool SyncFocuser(uint32_t ticks) override;

        /**
         * @brief SetFocuserSpeed Set Focuser speed
         * @param speed focuser speed
         * @return true if successful, false otherwise
         */
        virtual bool SetFocuserSpeed(int speed) override;

        /**
         * @brief SetFocuserBacklash Set the focuser backlash compensation value
         * @param steps value in absolute steps to compensate
         * @return True if successful, false otherwise.
         */
        virtual bool SetFocuserBacklash(int32_t steps);

        /**
         * @brief SetFocuserBacklashEnabled Enables or disables the focuser backlash compensation
         * @param enable flag to enable or disable backlash compensation
         * @return True if successful, false otherwise.
         */
        virtual bool SetFocuserBacklashEnabled(bool enabled);

        /**
         * @brief AbortFocuser all focus motion
         * @return True if abort is successful, false otherwise.
         */
        virtual bool AbortFocuser() override;

        int PortFD = -1;
    private:
        uint8_t swirlyConnection = CONNECTION_SERIAL | CONNECTION_TCP;
        bool callHandshake();

        bool Ack();

        // Get initial focuser parameter when we first connect
        void GetParams();
        // Read and update Step Mode
//        bool readStepMode();
        // Read and update Position
        bool readPosition();
        // Read and update speed
        bool readSpeed();
        // Read and update backlash
        bool readBacklash();
        // Read version
        bool readVersion();
        // Are we moving?
        bool isMoving();

        bool MoveFocuser(uint32_t position);
//        bool setStepMode(FocusStepMode mode);
        bool setSpeed(int speed);
        bool setBacklash(int step);
        void timedMoveCallback();

        uint32_t targetPos { 0 }, lastPos { 0 };

        // Full/Half Step modes
//        ISwitch StepModeS[2];
//        ISwitchVectorProperty StepModeSP;

        // Swirly Buffer
        static const uint8_t ML_RES { 32 };
        // Swirly Delimeter
        static const char ML_DEL { '#' };
        // Swirly Timeout
        static const uint8_t ML_TIMEOUT { 3 };
};
