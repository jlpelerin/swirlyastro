/*
 * Swirly_utils.cpp
 *
 *  Created on: 5 oct. 2020
 *      Author: jep
 */

#include "SwirlyDevice.h"

#include <indicom.h>
#include <connectionplugins/connectionserial.h>
#include <connectionplugins/connectiontcp.h>
#include <indilogger.h>
#include <termios.h>

#include <unistd.h>

using namespace std;

SwirlyDevice::SwirlyDevice() {
	debugStream.open("/tmp/Swirly.log");
}

bool
SwirlyDevice::sendCommand(int PortFD, const char * cmd, char * res, int nret)
{
//    LOG_DEBUG("SwirlyDevice::sendCommand(...)");

    int nbytes_written = 0, nbytes_read = 0, rc = -1;

    tcflush(PortFD, TCIOFLUSH);

//    LOGF_DEBUG("  CMD <%s>", cmd);

    if ((rc = tty_write_string(PortFD, cmd, &nbytes_written)) != TTY_OK)
    {
        char errstr[MAXRBUF] = {0};
        tty_error_msg(rc, errstr, MAXRBUF);
//        if (!silent)
        LOGF_ERROR("  Serial write error: %s.", errstr);
        return false;
    }

    if (res == nullptr)
        return true;

    // this is to handle the GV command which doesn't return the terminator, use the number of chars expected
    if (nret == 0)
    {
//        LOG_DEBUG("  Reading result until '#'");
        rc = tty_nread_section(PortFD, res, ML_RES, ML_DEL, ML_TIMEOUT, &nbytes_read);
    }
    else
    {
//        LOGF_DEBUG("  Reading %d bytes", nret);
        rc = tty_read(PortFD, res, nret, ML_TIMEOUT, &nbytes_read);
    }
    if (rc != TTY_OK)
    {
        char errstr[MAXRBUF] = {0};
        tty_error_msg(rc, errstr, MAXRBUF);
        LOGF_ERROR("  Serial read error: %s.", errstr);
        return false;
    }

//    LOGF_DEBUG("  RES <%s>", res);

    tcflush(PortFD, TCIOFLUSH);

    return true;
}
