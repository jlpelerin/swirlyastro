/*
 * Swirly_FlipFlat.h
 *
 *  Created on: 4 oct. 2020
 *      Author: jep
 */

#ifndef SRC_SWIRLY_FLIPFLAT_H_
#define SRC_SWIRLY_FLIPFLAT_H_

#include "indifocuserinterface.h"
#include "indilightboxinterface.h"
#include "indidustcapinterface.h"

#include <stdint.h>

#include "SwirlyDevice.h"

namespace Connection
{
class Serial;
}

class Swirly_FlipFlat :
		public SwirlyDevice,
		public INDI::LightBoxInterface,
		public INDI::DustCapInterface
{
    public:
		typedef enum {
			DUSTCAP_OPEN = 0,
			DUSTCAP_CLOSED = 1,
			DUSTCAP_UNDEFINED = 2,
		} DustCapStatus;
		typedef enum {
			MOTOR_OFF = 0,
			MOTOR_ON = 1,
			MOTOR_UNDEFINED = 2
		} MotorStatus;
		typedef enum {
			LIGHT_OFF = 0,
			LIGHT_ON = 1,
			LIGHT_UNDEFINED = 2
		} LightStatus;

        Swirly_FlipFlat();
        virtual ~Swirly_FlipFlat() = default;

        virtual bool initProperties();
        virtual void ISGetProperties(const char *dev);
        virtual bool updateProperties();

        virtual bool ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n);
        virtual bool ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n);
        virtual bool ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n);
        virtual bool ISSnoopDevice(XMLEle *root);

        static void parkTimeoutHelper(void *context);
        static void unparkTimeoutHelper(void *context);
    protected:
        const char *getDefaultName();

        virtual bool saveConfigItems(FILE *fp);
        void TimerHit();

        // From Dust Cap
        virtual IPState ParkCap();
        virtual IPState UnParkCap();

        // From Light Box
        virtual bool SetLightBoxBrightness(uint16_t value);
        virtual bool EnableLightBox(bool enable);

    private:
        bool getStartupData();
        bool Ack();
        // Read version
        bool readVersion();
        bool getStatus();
        bool getBrightness();

        void parkTimeout();
        int parkTimeoutID { -1 };
        void unparkTimeout();
        int unparkTimeoutID { -1 };

        bool callHandshake();
        bool Handshake();

        // Status
        ITextVectorProperty StatusTP;
        IText StatusT[3] {};

        // Firmware version
        ITextVectorProperty FirmwareTP;
        IText FirmwareT[1] {};

        int PortFD{ -1 };
        uint16_t productID{ 0 };

        uint8_t simulationWorkCounter{ 0 };
        DustCapStatus prevDustCapStatus{ DUSTCAP_UNDEFINED };
        LightStatus prevLightStatus{ LIGHT_UNDEFINED };
        MotorStatus prevMotorStatus{ MOTOR_UNDEFINED };
        uint8_t prevBrightness{ 0xFF };

        Connection::Serial *serialConnection{ nullptr };
};



#endif /* SRC_SWIRLY_FLIPFLAT_H_ */
