#include "Swirly_FlipFlat.h"

#include "indicom.h"
#include "connectionplugins/connectionserial.h"

#include <cerrno>
#include <cstring>
#include <memory>
#include <termios.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <execinfo.h>

// We declare an auto pointer to FlipFlat.
static std::unique_ptr<Swirly_FlipFlat> flipflat(new Swirly_FlipFlat());

#define FLAT_TIMEOUT 3

void ISGetProperties(const char *dev)
{
    flipflat->ISGetProperties(dev);
}

void ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n)
{
    flipflat->ISNewSwitch(dev, name, states, names, n);
}

void ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n)
{
    flipflat->ISNewText(dev, name, texts, names, n);
}

void ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
    flipflat->ISNewNumber(dev, name, values, names, n);
}

void ISNewBLOB(const char *dev, const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[],
               char *names[], int n)
{
    INDI_UNUSED(dev);
    INDI_UNUSED(name);
    INDI_UNUSED(sizes);
    INDI_UNUSED(blobsizes);
    INDI_UNUSED(blobs);
    INDI_UNUSED(formats);
    INDI_UNUSED(names);
    INDI_UNUSED(n);
}

void
ISSnoopDevice(XMLEle *root)
{
    flipflat->ISSnoopDevice(root);
}

Swirly_FlipFlat::Swirly_FlipFlat() :
		SwirlyDevice(),
		LightBoxInterface(this, false) // Not dimmable
{
    setVersion(1, 1);
    debug() << "Let's go" << std::endl;
}

bool
Swirly_FlipFlat::initProperties()
{
    INDI::DefaultDevice::initProperties();

    // Status
    IUFillText(&StatusT[0], "Cover", "Cover", nullptr);
    IUFillText(&StatusT[1], "Light", "Light", nullptr);
    IUFillText(&StatusT[2], "Motor", "Motor", nullptr);
    IUFillTextVector(&StatusTP, StatusT, 3, getDeviceName(), "Status", "Status", MAIN_CONTROL_TAB, IP_RO, 60, IPS_IDLE);

    // Firmware version
    IUFillText(&FirmwareT[0], "Version", "Version", nullptr);
    IUFillTextVector(&FirmwareTP, FirmwareT, 1, getDeviceName(), "Firmware", "Firmware", MAIN_CONTROL_TAB, IP_RO, 60, IPS_IDLE);

    initDustCapProperties(getDeviceName(), MAIN_CONTROL_TAB);
    initLightBoxProperties(getDeviceName(), MAIN_CONTROL_TAB);

    LightIntensityN[0].min  = 0;
    LightIntensityN[0].max  = 255;
    LightIntensityN[0].step = 10;

    setDriverInterface(AUX_INTERFACE | LIGHTBOX_INTERFACE | DUSTCAP_INTERFACE);

    addAuxControls();

    serialConnection = new Connection::Serial(this);
    serialConnection->registerHandshake([&]()
    {
        return callHandshake();
    });
    DefaultDevice::registerConnection(serialConnection);

    return true;
}

void Swirly_FlipFlat::ISGetProperties(const char *dev)
{
    INDI::DefaultDevice::ISGetProperties(dev);

    // Get Light box properties
    isGetLightBoxProperties(dev);
}

bool Swirly_FlipFlat::updateProperties()
{
    INDI::DefaultDevice::updateProperties();

    if (isConnected())
    {
    	defineSwitch(&ParkCapSP);
        defineSwitch(&LightSP);
        defineNumber(&LightIntensityNP);
        defineText(&StatusTP);
        defineText(&FirmwareTP);

        updateLightBoxProperties();

        getStartupData();
    }
    else
    {
    	deleteProperty(ParkCapSP.name);
        deleteProperty(LightSP.name);
        deleteProperty(LightIntensityNP.name);
        deleteProperty(StatusTP.name);
        deleteProperty(FirmwareTP.name);

        updateLightBoxProperties();
    }

    return true;
}

const char *Swirly_FlipFlat::getDefaultName()
{
    return "Swirly Flip-Flat";
}

bool
Swirly_FlipFlat::callHandshake()
{
    if (isSimulation())
    {
        LOGF_INFO("Connected successfuly to simulated %s. Retrieving startup data...", getDeviceName());

        SetTimer(POLLMS);

        setDriverInterface(AUX_INTERFACE | LIGHTBOX_INTERFACE | DUSTCAP_INTERFACE);

        return true;
    }

  //  if (swirlyConnection > 0)
//    {
        if (getActiveConnection() == serialConnection)
            PortFD = serialConnection->getPortFD();
//        else if (getActiveConnection() == tcpConnection)
//            PortFD = tcpConnection->getPortFD();
//    }

    return Handshake();
}

bool Swirly_FlipFlat::Handshake()
{
    if (Ack())
    {
        LOG_INFO("Swirly Flip-Flat is online. Getting parameters...");
        return true;
    }

    LOG_ERROR("Device ping failed.");
    return false;
}

bool
Swirly_FlipFlat::ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
    if (processLightBoxNumber(dev, name, values, names, n))
        return true;

    return INDI::DefaultDevice::ISNewNumber(dev, name, values, names, n);
}

bool
Swirly_FlipFlat::ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n)
{
    if (dev != nullptr && strcmp(dev, getDeviceName()) == 0)
    {
        if (processLightBoxText(dev, name, texts, names, n))
            return true;
    }

    return INDI::DefaultDevice::ISNewText(dev, name, texts, names, n);
}

bool
Swirly_FlipFlat::ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n)
{
    if (dev != nullptr && strcmp(dev, getDeviceName()) == 0)
    {
        if (processDustCapSwitch(dev, name, states, names, n))
            return true;

        if (processLightBoxSwitch(dev, name, states, names, n))
            return true;
    }

    return INDI::DefaultDevice::ISNewSwitch(dev, name, states, names, n);
}

bool
Swirly_FlipFlat::ISSnoopDevice(XMLEle *root)
{
    snoopLightBox(root);

    return INDI::DefaultDevice::ISSnoopDevice(root);
}

bool
Swirly_FlipFlat::saveConfigItems(FILE *fp)
{
    INDI::DefaultDevice::saveConfigItems(fp);

    return saveLightBoxConfigItems(fp);
}

bool
Swirly_FlipFlat::Ack()
{
    bool success = false;

    for (int i = 0; i < 3; i++)
    {
        if (readVersion())
        {
            success = true;
            setDriverInterface(AUX_INTERFACE | LIGHTBOX_INTERFACE | DUSTCAP_INTERFACE);
            break;
        }

        sleep(1);
    }

    return success;
}

bool
Swirly_FlipFlat::readVersion()
{
    char res[ML_RES] = {0};

    if (sendCommand(PortFD, "CGV#", res, 3) == false)
        return false;

    LOGF_INFO("Detected firmware version %c.%c", res[0], res[1]);
    LOGF_EXTRA1("Detected firmware version %c.%c", res[0], res[1]);

    return true;
}

bool Swirly_FlipFlat::getStartupData()
{
    LOG_INFO("Swirly_FlipFlat::getStartupData()");
    LOG_EXTRA1("Swirly_FlipFlat::getStartupData()");

    bool rc2 = getStatus();
    bool rc3 = getBrightness();

    return (rc2 && rc3);
}

IPState Swirly_FlipFlat::ParkCap()
{
//    LOG_DEBUG("Swirly_FlipFlat::ParkCap");

    if (isSimulation())
    {
        simulationWorkCounter = 3;
        return IPS_BUSY;
    }

    LOG_INFO("Swirly Flip-Flat is parking");

    if (!sendCommand(PortFD, "CFC#")) {
        LOG_DEBUG("Failed to send parking command");
        return IPS_ALERT;
    }

//    prevDustCapStatus = DUSTCAP_UNDEFINED;

    IERmTimer(parkTimeoutID);
    parkTimeoutID = IEAddTimer(30000, parkTimeoutHelper, this);

    return IPS_BUSY;
}

IPState Swirly_FlipFlat::UnParkCap()
{
    if (isSimulation())
    {
        simulationWorkCounter = 3;
        return IPS_BUSY;
    }

    LOG_INFO("Swirly Flip-Flat is unparking");

    if (!sendCommand(PortFD, "CFO#"))
        return IPS_ALERT;

//    prevDustCapStatus = DUSTCAP_UNDEFINED;

    IERmTimer(unparkTimeoutID);
    unparkTimeoutID = IEAddTimer(30000, unparkTimeoutHelper, this);

    return IPS_BUSY;
}

bool Swirly_FlipFlat::EnableLightBox(bool enable)
{
    char command[ML_RES];
    char response[ML_RES];

    if (ParkCapS[1].s == ISS_ON)
    {
        LOG_ERROR("Cannot control light while cap is unparked.");
        return false;
    }

    if (isSimulation())
        return true;

    if (enable) {
        LOG_INFO("Swirly Flip-Flat is switching light on");
        strncpy(command, "LFO#", ML_RES);
    } else {
        LOG_INFO("Swirly Flip-Flat is switching light off");
        strncpy(command, "LFX#", ML_RES);
    }
    if (!sendCommand(PortFD, command))
        return false;

    return true;
}

bool Swirly_FlipFlat::getStatus()
{
    char response[ML_RES];
    MotorStatus motorStatus;
    LightStatus lightStatus;
    DustCapStatus dustCapStatus;

    if (isSimulation())
    {
        if (ParkCapSP.s == IPS_BUSY && --simulationWorkCounter <= 0)
        {
            ParkCapSP.s = IPS_OK;
            IDSetSwitch(&ParkCapSP, nullptr);
            simulationWorkCounter = 0;
        }

        if (ParkCapSP.s == IPS_BUSY)
        {
            motorStatus = MOTOR_ON;
            dustCapStatus = DUSTCAP_UNDEFINED;
        }
        else
        {
            motorStatus = MOTOR_OFF;
            // Parked/Closed
            if (ParkCapS[CAP_PARK].s == ISS_ON)
                dustCapStatus = DUSTCAP_CLOSED;
            else
                dustCapStatus = DUSTCAP_OPEN;
        }

        lightStatus = (LightS[FLAT_LIGHT_ON].s == ISS_ON) ? LIGHT_ON : LIGHT_OFF;
    }
    else
    {
        {
        	char ret[ML_RES] = {0};

        	if (!sendCommand(PortFD, "CGM#", ret, 2)) {
        		LOG_ERROR("Swirly flip-flat: Failed to get motor status");
        		LOGF_ERROR("Motor status = '%s'", ret);
        		return false;
        	}

        	motorStatus = (ret[0] == '1') ? MOTOR_ON : MOTOR_OFF;
        }

        {
            char ret[ML_RES] = {0};
        	// TODO: if motor is moving then status must be undefined... no need to query
        	if (!sendCommand(PortFD, "CGS#", ret, 2)) {
        		LOG_ERROR("Swirly flip-flat: Failed to get cap status");
        		return false;
        	}

            switch(ret[0]) {
        	case '0': dustCapStatus = DUSTCAP_OPEN; break;
        	case '1': dustCapStatus = DUSTCAP_CLOSED; break;
        	default: dustCapStatus = DUSTCAP_UNDEFINED; break;
        	}

            debug() << "ret[0] = '" << ret[0] << "'" << std::endl; debug().flush();
            debug() << "dustCapSatus = '" << dustCapStatus << "'" << std::endl; debug().flush();
        }

        {
            char ret[ML_RES] = {0};
        	if (!sendCommand(PortFD, "LGS#", ret, 0)) {
        		LOG_ERROR("Swirly flip-flat: Failed to get light status");
        		return false;
        	}

        	lightStatus = (ret[0] == '1') ? LIGHT_ON : LIGHT_OFF;
        }
    }

    bool statusUpdated = false;

    if (dustCapStatus != prevDustCapStatus)
    {
        debug() << "Dustcap status changed" << std::endl; debug().flush();

        prevDustCapStatus = dustCapStatus;

        statusUpdated = true;

        switch (dustCapStatus)
        {
            case DUSTCAP_OPEN:
                IUSaveText(&StatusT[0], "Open");
                if (ParkCapSP.s == IPS_BUSY || ParkCapSP.s == IPS_IDLE)
                {
                    IUResetSwitch(&ParkCapSP);
                    ParkCapS[1].s = ISS_ON;
                    ParkCapSP.s   = IPS_OK;
                    LOG_INFO("Cover open.");
                    IDSetSwitch(&ParkCapSP, nullptr);
                }
                break;

            case DUSTCAP_CLOSED:
                IUSaveText(&StatusT[0], "Closed");
                if (ParkCapSP.s == IPS_BUSY || ParkCapSP.s == IPS_IDLE)
                {
                    IUResetSwitch(&ParkCapSP);
                    ParkCapS[0].s = ISS_ON;
                    ParkCapSP.s   = IPS_OK;
                    LOG_INFO("Cover closed.");
                    IDSetSwitch(&ParkCapSP, nullptr);
                }
                break;

            case DUSTCAP_UNDEFINED:
                debug() << "Updating dustCapSatus to UNDEFINED" << std::endl; debug().flush();
                IUSaveText(&StatusT[0], "Not Open/Closed");
                IUResetSwitch(&ParkCapSP);
                ParkCapS[0].s = ISS_OFF;
                ParkCapS[1].s = ISS_OFF;
                IDSetSwitch(&ParkCapSP, nullptr);
                break;

            case 3:
                IUSaveText(&StatusT[0], "Timed out");
                break;
        }
    }

    if (lightStatus != prevLightStatus)
    {
        prevLightStatus = lightStatus;

        statusUpdated = true;

        switch (lightStatus)
        {
            case 0:
                IUSaveText(&StatusT[1], "Off");
                if (LightS[0].s == ISS_ON)
                {
                    LightS[0].s = ISS_OFF;
                    LightS[1].s = ISS_ON;
                    IDSetSwitch(&LightSP, nullptr);
                }
                break;

            case 1:
                IUSaveText(&StatusT[1], "On");
                if (LightS[1].s == ISS_ON)
                {
                    LightS[0].s = ISS_ON;
                    LightS[1].s = ISS_OFF;
                    IDSetSwitch(&LightSP, nullptr);
                }
                break;
        }
    }

    if (motorStatus != prevMotorStatus)
    {
        prevMotorStatus = motorStatus;

        statusUpdated = true;

        switch (motorStatus)
        {
            case 0:
                IUSaveText(&StatusT[2], "Stopped");
                break;

            case 1:
                IUSaveText(&StatusT[2], "Running");
                break;
        }
    }

    if (statusUpdated)
        IDSetText(&StatusTP, nullptr);

    return true;
}

void Swirly_FlipFlat::TimerHit()
{
    if (!isConnected())
        return;

    getStatus();

    // parking or unparking timed out, try again
    if (ParkCapSP.s == IPS_BUSY && !strcmp(StatusT[0].text, "Timed out"))
    {
        if (ParkCapS[0].s == ISS_ON)
            ParkCap();
        else
            UnParkCap();
    }

    SetTimer(POLLMS);
}

bool Swirly_FlipFlat::getBrightness()
{
	return true;

    if (isSimulation())
    {
        return true;
    }

    char response[ML_RES] = {0};
    if (!sendCommand(PortFD, ">J000", response))
        return false;

    char brightnessString[4] = { 0 };
    snprintf(brightnessString, 4, "%s", response + 4);

    int brightnessValue = 0;
    int rc              = sscanf(brightnessString, "%d", &brightnessValue);

    if (rc <= 0)
    {
        LOGF_ERROR("Unable to parse brightness value (%s)", response);
        return false;
    }

    if (brightnessValue != prevBrightness)
    {
        prevBrightness           = brightnessValue;
        LightIntensityN[0].value = brightnessValue;
        IDSetNumber(&LightIntensityNP, nullptr);
    }

    return true;
}

bool Swirly_FlipFlat::SetLightBoxBrightness(uint16_t value)
{
    if (isSimulation())
    {
        LightIntensityN[0].value = value;
        IDSetNumber(&LightIntensityNP, nullptr);
        return true;
    }

    char command[ML_RES] = {0};
    char response[ML_RES] = {0};

    snprintf(command, ML_RES, ">B%03d", value);

    if (!sendCommand(PortFD, command, response))
        return false;

    char brightnessString[4] = { 0 };
    snprintf(brightnessString, 4, "%s", response + 4);

    int brightnessValue = 0;
    int rc              = sscanf(brightnessString, "%d", &brightnessValue);

    if (rc <= 0)
    {
        LOGF_ERROR("Unable to parse brightness value (%s)", response);
        return false;
    }

    if (brightnessValue != prevBrightness)
    {
        prevBrightness           = brightnessValue;
        LightIntensityN[0].value = brightnessValue;
        IDSetNumber(&LightIntensityNP, nullptr);
    }

    return true;
}

void Swirly_FlipFlat::parkTimeoutHelper(void *context)
{
    static_cast<Swirly_FlipFlat*>(context)->parkTimeout();
}

void Swirly_FlipFlat::unparkTimeoutHelper(void *context)
{
    static_cast<Swirly_FlipFlat*>(context)->unparkTimeout();
}

void Swirly_FlipFlat::parkTimeout()
{
    if (ParkCapSP.s == IPS_BUSY)
    {
        LOG_WARN("Parking cap timed out. Retrying...");
        ParkCap();
    }
}

void Swirly_FlipFlat::unparkTimeout()
{
    if (ParkCapSP.s == IPS_BUSY)
    {
        LOG_WARN("UnParking cap timed out. Retrying...");
        UnParkCap();
    }
}
